const mongoose = require('mongoose')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')

async function main () {
  const newInformaticsBuilding = await Building.findById('621ba1f5c1aa849112495e57')
  const room = await Room.findById('621ba1f5c1aa849112495e5c')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
